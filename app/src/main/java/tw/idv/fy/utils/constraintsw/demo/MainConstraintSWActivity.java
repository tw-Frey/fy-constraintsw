package tw.idv.fy.utils.constraintsw.demo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.Toast;

import tw.idv.fy.utils.constraintsw.webview.settings.ConstraintWebViewSetting;
import tw.idv.fy.utils.constraintsw.IConstraintSW;

/**
 * 使用 px-by-dip 縮放率
 */
public class MainConstraintSWActivity extends Activity implements IConstraintSW {

    @Override
    protected void attachBaseContext(android.content.Context newBase) {
        super.attachBaseContext(onBeforeAttachBaseContext(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        WebView webView = findViewById(R.id.webview);
        webView.getSettings().setLoadWithOverviewMode(false); // default: false
        webView.getSettings().setUseWideViewPort(false);
        webView.setBackgroundColor(Color.argb(128, 255, 0, 0));
        /*
         * 使用 px-by-dip 縮放率
         */
        webView.setInitialScale(ConstraintWebViewSetting.SuggestInitialScale2());
        webView.loadUrl("file:///android_asset/hello.html");

        new AlertDialog.Builder(this)
                .setTitle(R.string.app_name)
                .setMessage(R.string.hello_world)
                .setPositiveButton(android.R.string.ok,
                        (dialog, which) -> startActivity(new Intent(MainConstraintSWActivity.this, SecondActivity.class))
                ).show();
        Toast.makeText(getApplicationContext(), R.string.hello_world, Toast.LENGTH_LONG).show();
    }

}
