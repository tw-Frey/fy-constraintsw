package tw.idv.fy.utils.constraintsw.demo;

import android.app.Application;
import android.os.Build;
import android.webkit.WebView;

import tw.idv.fy.utils.constraintsw.IConstraintSW;

public class App extends Application implements IConstraintSW {

    @Override
    protected void attachBaseContext(android.content.Context base) {
        super.attachBaseContext(onBeforeAttachBaseContext(base));
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
    }
}
