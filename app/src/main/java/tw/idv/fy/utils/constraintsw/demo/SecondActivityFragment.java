package tw.idv.fy.utils.constraintsw.demo;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import tw.idv.fy.utils.constraintsw.webview.settings.ConstraintWebViewSetting;

/**
 * 使用 px-by-measure 縮放率
 * or
 * 使用 px-by-dip(scaled) 縮放率
 */
public class SecondActivityFragment extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        WebView webView = view.findViewById(R.id.webview);
        webView.getSettings().setLoadWithOverviewMode(false); // default: false
        webView.getSettings().setUseWideViewPort(false);
        webView.setBackgroundColor(Color.argb(128, 255, 0, 0));
//        /*
//         * 使用 px-by-measure 縮放率
//         */
//        webView.setInitialScale(ConstraintWebViewSetting.SuggestInitialScale3());
        /*
         * 使用 px-by-dip(scaled) 縮放率; 依賴 DisplayMetric; 它也許 scaled
         */
        webView.setInitialScale(ConstraintWebViewSetting.SuggestInitialScale4(view.getContext()));
        webView.loadUrl("file:///android_asset/hello.html");
    }

}
