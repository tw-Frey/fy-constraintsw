package tw.idv.fy.utils.constraintsw.webview.settings;

/*package*/ interface IWebViewSetting {

    IWebViewSetting Default = new IWebViewSetting() {};

    default int suggestInitialScale() {
        return 0;
    }

}
