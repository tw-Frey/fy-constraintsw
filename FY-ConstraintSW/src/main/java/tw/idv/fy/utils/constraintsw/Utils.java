package tw.idv.fy.utils.constraintsw;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;

/*package*/ class Utils {

    /*package*/ static Configuration OverrideConfiguration(@NonNull Context context, @NonNull Configuration configuration) {
        if (BuildConfig.DEBUG) android.util.Log.v("Configuration-Faty", configuration.toString());
        if (MetaUnScaledFont.Check(context)) configuration.fontScale = 1; // 避免縮放字體
        float smallWidth = MetaSmallWidthDp.Gain(context);
        float scale = smallWidth / configuration.smallestScreenWidthDp;
        if (scale > 1) {
            // 設定 新配置
            Configuration newConfiguration = new Configuration(configuration);
            newConfiguration.smallestScreenWidthDp = (int) smallWidth;
            newConfiguration.densityDpi     = Math.round(newConfiguration.densityDpi / scale);
            newConfiguration.screenWidthDp  = Math.round(newConfiguration.screenWidthDp * scale);
            newConfiguration.screenHeightDp = Math.round(newConfiguration.screenHeightDp * scale);
            newConfiguration.screenLayout &= ~Configuration.SCREENLAYOUT_SIZE_MASK;
            newConfiguration.screenLayout |=  Configuration.SCREENLAYOUT_SIZE_XLARGE;
            return newConfiguration;
        }
        return configuration;
    }

    private static class MetaSmallWidthDp {
        /**
         * 在 AndroidManifest 裡指定的 key-name
         */
        private static final String META_SW_KEY = "tw.idv.fy.utils.ConstraintSW";
        /**
         * Small Width 預設 768(dp)
         */
        private static final float DEFAULT = 768f;
        /**
         * 單體 (volatile singleton)
         */
        private static volatile Float SW = null;
        /**
         *  取得 small width
         */
        private static float Gain(@NonNull Context context) {
            if (SW == null) {
                synchronized (MetaSmallWidthDp.class) {
                    if (SW == null) {
                        try {
                            ApplicationInfo appInfo = context.getPackageManager()
                                     .getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
                            Bundle metaData = appInfo.metaData;
                            int sw = metaData.getInt(META_SW_KEY, 0);
                            if (sw > 0) SW = (float) sw;
                            else throw new IllegalArgumentException("Meta 未設 small width; 將預設為 768dp");
                        } catch (Throwable e) {
                            e.printStackTrace();
                            SW = DEFAULT;
                        }
                    }
                }
            }
            return SW;
        }
    }

    private static class MetaUnScaledFont {
        /**
         * 在 AndroidManifest 裡指定的 key-name
         */
        private static final String META_UNSCALED_KEY = "tw.idv.fy.utils.UnScaledFont";
        /**
         * 是否 UnScaledFont, 預設: true
         */
        private static final boolean DEFAULT = true;
        /**
         * 單體 (volatile singleton)
         */
        private static volatile Boolean UnScaledFont = null;
        /**
         *  取得 是否 UnScaledFont
         */
        private static boolean Check(@NonNull Context context) {
            if (UnScaledFont == null) {
                synchronized (MetaSmallWidthDp.class) {
                    if (UnScaledFont == null) {
                        try {
                            ApplicationInfo appInfo = context.getPackageManager()
                                     .getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
                            Bundle metaData = appInfo.metaData;
                            UnScaledFont = metaData.getBoolean(META_UNSCALED_KEY, DEFAULT);
                        } catch (Throwable e) {
                            e.printStackTrace();
                            UnScaledFont = DEFAULT;
                        }
                    }
                }
            }
            return UnScaledFont;
        }
    }

}
