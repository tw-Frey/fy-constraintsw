package tw.idv.fy.utils.constraintsw.webview.settings;

import android.content.Context;
import android.support.annotation.NonNull;

/*package*/ class WebViewSetting4 implements IWebViewSetting {

    private static volatile Integer ScaleInitialScale = null;

    /*package*/ WebViewSetting4() {}

    public IWebViewSetting init(@NonNull Context context) {
        if (ScaleInitialScale == null) {
            synchronized (this) {
                if (ScaleInitialScale == null) {
                    try {
                        ScaleInitialScale = (int) (100 * context.getResources().getDisplayMetrics().scaledDensity);
                    } catch (Throwable e) {
                        e.printStackTrace();
                        ScaleInitialScale = ConstraintWebViewSetting.SuggestInitialScale1();
                    }
                }
            }
        }
        return this;
    }

    @Override
    public int suggestInitialScale() {
        if (ScaleInitialScale == null) throw new RuntimeException("need init() first");
        return ScaleInitialScale;
    }
}
