package tw.idv.fy.utils.constraintsw;

import android.content.Context;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.view.ContextThemeWrapper;

public interface IConstraintSW {

    default Context onBeforeAttachBaseContext(@NonNull Context base) {
        Configuration configuration = Utils.OverrideConfiguration(base, base.getResources().getConfiguration());
        if (BuildConfig.DEBUG) android.util.Log.i("Configuration-Faty", configuration.toString());
        if (this instanceof ContextThemeWrapper) {
            // 通常是 Activity 才會進來這裡
            ((ContextThemeWrapper) this).applyOverrideConfiguration(configuration);
            return base;
        }
        // 通常是 Application 才會來到這裡
        return base.createConfigurationContext(configuration);
    }

}
