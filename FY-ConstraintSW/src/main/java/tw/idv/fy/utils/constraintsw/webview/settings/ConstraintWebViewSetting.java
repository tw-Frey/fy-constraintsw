package tw.idv.fy.utils.constraintsw.webview.settings;

import android.content.Context;
import android.support.annotation.NonNull;

public class ConstraintWebViewSetting {

    private static class Singleton0 {
        private static final IWebViewSetting WebViewSetting = IWebViewSetting.Default;
    }

    private static class Singleton1 {
        private static final IWebViewSetting WebViewSetting = new WebViewSetting1();
    }

    private static class Singleton2 {
        private static final IWebViewSetting WebViewSetting = new WebViewSetting2();
    }

    private static class Singleton3 {
        private static final IWebViewSetting WebViewSetting = new WebViewSetting3();
    }

    private static class Singleton4 {
        private static final WebViewSetting4 WebViewSetting = new WebViewSetting4();
    }

    /**
     *  系統決定的縮放率
     */
    public static int SuggestInitialScale0() {
        return Singleton0.WebViewSetting.suggestInitialScale();
    }

    /**
     * 真實 px 對應的縮放率;
     */
    public static int SuggestInitialScale1() {
        return Singleton1.WebViewSetting.suggestInitialScale();
    }

    /**
     * px-by-dip 縮放率; 一般來說, 會等於 SuggestInitialScale0()
     */
    public static int SuggestInitialScale2() {
        return Singleton2.WebViewSetting.suggestInitialScale();
    }

    /**
     *  px-by-measure 縮放率; 例如 不同螢幕大小上 26px 的字都一致大
     */
    public static int SuggestInitialScale3() {
        return Singleton3.WebViewSetting.suggestInitialScale();
    }

    /**
     * px-by-dip(scaled) 縮放率; 依賴 DisplayMetric; 它也許 scaled
     */
    public static int SuggestInitialScale4(@NonNull Context context) {
        return Singleton4.WebViewSetting.init(context).suggestInitialScale();
    }
}
