package tw.idv.fy.utils.constraintsw.webview.settings;

import android.content.res.Resources;

/*package*/ class WebViewSetting2 implements IWebViewSetting {

    private final int scaleInitialScale;

    /*package*/ WebViewSetting2() {
        int scaleInitialScale = 0;
        try {
            scaleInitialScale = (int) (100 * Resources.getSystem().getDisplayMetrics().scaledDensity);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        this.scaleInitialScale = scaleInitialScale;
    }

    @Override
    public int suggestInitialScale() {
        return scaleInitialScale;
    }
}
