package tw.idv.fy.utils.constraintsw.webview.settings;

import android.content.res.Resources;

/*package*/ class WebViewSetting3 implements IWebViewSetting {

    private final int scaleInitialScale;

    /*package*/ WebViewSetting3() {
        int scaleInitialScale = 0;
        try {
            scaleInitialScale = (int) (100 * (Resources.getSystem().getDisplayMetrics().xdpi / 160f) + 0.5f);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        this.scaleInitialScale = scaleInitialScale;
    }

    @Override
    public int suggestInitialScale() {
        return scaleInitialScale;
    }
}
